import React, { Component } from 'react';
import logo from '../images/logo.png';
import family1 from '../images/family1.png';
import family2 from '../images/family2.jpg';
import '../styles/css/App.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <nav class="site-navbar navbar navbar-expand-lg navbar-light">
          <div class="container">
              <a class="navbar-brand" href="/">
                <img class="site-logo" src={logo} alt="FamilyMatch.com" />
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="site-nav collapse navbar-collapse" id="navbarSupportedContent">
                <form class="form-inline my-2 my-lg-0">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                </form>
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="site-nav-item nav-link" href="/about">About</a>
                  </li>
                  <li class="nav-item">
                    <a class="site-nav-item nav-link" href="/press">Press</a>
                  </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="site-nav-item btn btn-primary" href="/signin">Sign In</a>
                  </li>
                </ul>
              </div>
          </div>
        </nav>
        <section class="site-home">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-12">
                <div class="mt-5 mb-5">
                  <h1 class="display-4">Complete your family</h1>
                  <h3 class="text-light text-muted mb-4">Start by telling us about yourself</h3>
                  
                  <form>
                    <legend class="text-primary">About You</legend>
                    <div class="form-signup-section">
                      <div class="row">
                        <div class="col-10">
                          <div class="row">
                            <div class="col-8">
                              <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control">
                                  <option>Male</option>
                                  <option>Female</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-4">
                              <label>Number of Children</label>
                              <select class="form-control">
                                <option>None</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>5+</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <legend class="text-primary">Looking for partner with</legend>
                    <div class="form-signup-section">
                      <div class="row">
                        <div class="col-10">
                          <div class="row">
                            <div class="col-8">
                              <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control">
                                  <option>Male</option>
                                  <option>Female</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-4">
                              <label>Number of Children</label>
                              <select class="form-control">
                                <option>None</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>5+</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button class="mt-3 btn-lg btn btn-primary">Find a Match</button>
                  </form>
                </div>
              </div>
              <div class="col-lg-6 d-lg-block d-none">
                <div class="card-images mr-auto">
                  <div class="card-image">
                    <img width="460" src={family2} alt="Family matched by FamilyMatch" />
                  </div>
                  <div class="card-image on-top">
                    <img src={family1} alt="Family matched by FamilyMatch" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default App;
